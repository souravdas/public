import java.util.ArrayList;


public class VotingMachine {

	private ArrayList <ElectedOffice> electedOffices;
	private ArrayList <ArrayList <Integer>> voteCount;
	
	public VotingMachine(ArrayList <ElectedOffice> eOffices) {
		
		electedOffices = eOffices;

		voteCount = new ArrayList<>();
		
		for (int oIndex = 0; oIndex < electedOffices.size(); oIndex++) {
			ArrayList<Integer> v = new ArrayList<>();
			for (int j = 0; j < electedOffices.get(oIndex).getCandiateCount(); j++) {
				v.add(0);
			}
			voteCount.add(v);
		}
	}
	
	public ArrayList <ElectedOffice> getElectionOffices(){
		return electedOffices;
	}
	
	public int getOfficeCount() {
		return electedOffices.size();
	}
	
	public ElectedOffice getElectedOfficeAt(int index) {
		return electedOffices.get(index);
	}
	
	public Ballot getNewBallot() {
		return new Ballot(electedOffices);
	}
	
	public void tallyBallot(Ballot b) {
		for (int o = 0; o < voteCount.size(); o++) {
			for (int c = 0; c < b.getCandidateCountForOffice(o); c++) {
				if (b.getVoteForCandidate(o, c)) {
					int v = voteCount.get(o).get(c);
					voteCount.get(o).set(c, v+1);
				}
			}
		}
	}
	
	public int reportForCandidate(int oIndex, int cIndex) {
		return voteCount.get(oIndex).get(cIndex);
	}
}
