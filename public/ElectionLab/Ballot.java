import java.util.ArrayList;


public class Ballot {
	
	public final ArrayList <ElectedOffice> electedOffices;
	
	private ArrayList <ArrayList<Boolean>> vote;
		
	public Ballot(ArrayList <ElectedOffice> eOffices) {
		
		electedOffices = eOffices;
		
		vote = new ArrayList<ArrayList<Boolean>>();
		for (int i = 0; i < eOffices.size(); i++) {
			ArrayList <Boolean> v = new ArrayList<>();
			for (int j = 0; j < eOffices.get(i).getCandiateCount(); j++)
				v.add(false);
			vote.add(v);
		}
	}
	
	public String toString() {
		String ret = "";
		for (ElectedOffice office: electedOffices) {
			ret += office + "\n\n";
		}
		return ret;
	}
	
	public int getOfficeCount() {
		return electedOffices.size();
	}
	
	public ElectedOffice getOfficeAt(int index) {
		return electedOffices.get(index);
	}
	
	public int getCandidateCountForOffice(int officeIndex) {
		return electedOffices.get(officeIndex).getCandiateCount();
	}
	
	public Candidate getCandidateForOffice(int oIndex, int cIndex) {
		return electedOffices.get(oIndex).getCandidateAt(cIndex);
	}
	
	public void castVoteForCandidate(int oIndex, int cIndex) {
		vote.get(oIndex).set(cIndex, true);
	}
	
	public boolean getVoteForCandidate(int oIndex, int cIndex) {
		return vote.get(oIndex).get(cIndex);
	}
}
