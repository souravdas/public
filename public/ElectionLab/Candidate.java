///////////////////////////////////////////////////////////////////////////////
//
// Title:            Candidate Class
// File:             Candidate.java
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * This class is used to store information about a candidate running for office.
 * 
 * @author George Wanant modified by Jim Skrentny 4/2010, 11/2011
 */
public class Candidate {

	private String name;			//Name of candidate
	private String party;			//Candidate's party
	private String office;			//Office candidate is seeking
	private boolean incumbent;		//Incumbent status of candidate
	
	/**         
	 * This minimal constructor requires a name and party.  The Candidate
	 * is assumed to not be currently seeking an office and not be an
	 * incumbent.
	 *
	 * @param name Name of the candidate
	 * @param party Name of the candidate's party
	 */
	public Candidate(String name, String party) {
		this.name = name;
		this.party = party;
		this.office = ""; 
		this.incumbent = false;
	}

	/**
	 * This constructor behaves the same as the two parameter constructor,
	 * but requires an office and incumbent status to be specified.
	 *
	 * @param name Name of the candidate
	 * @param party Name of the candidate's party
	 * @param office Office the candidate is seeking
	 * @param incumbent True if the candidate is an incumbent
	 */
	public Candidate(String name, String party, String office, 
			boolean incumbent) {
		this.name = name;
		this.party = party;
		this.office = office;
		this.incumbent = incumbent;
	}

	/**
	 * Accessor for candidate's name
	 *
	 * @return Name of the candidate
	 */
	public String getName() {
		return name;
	}

	/**
	 * Mutator for the candidate's name
	 *
	 * @param name New name for the candidate
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**         
	 * Accessor for the name of the candidate's party
	 *
	 * @return Name of the candidate's party
	 */
	public String getParty() {
		return this.party;
	}

	/**
	 * Mutator for the candidate's party
	 *
	 * @param party New name for the candidate's party
	 */
	public void setParty(String party) {
		this.party = party;
	}

	/**         
	 * Accessor for the office the candidate is seeking
	 *
	 * @return Office the candidate is seeking
	 */
	public String getOffice() {
		return this.office;
	}

	/**
	 * Mutator for the candidate's office
	 *
	 * @param office New name for the candidate's office
	 */
	public void setOffice(String office) {
		this.office = office;
	}

	/**         
	 * Accessor for candidate's incumbent status
	 *
	 * @return true if the candidate is an incumbent, false otherwise
	 */
	public boolean isIncumbent() {
		return this.incumbent;
	}

	/**
	 * Mutator for the candidate's incumbent status
	 *
	 * @param incumbent true if the candidate is an incumbent,
         * false otherwise
	 */
	public void setIncumbent(boolean incumbent) {
		this.incumbent = incumbent;
	}
	
}
