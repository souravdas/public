import java.util.ArrayList;


public class ElectedOffice {
	
	// Office name (Assuming won't change)
	public final String ELECTED_OFFICE;       
	
	// List of candidates contending for this office.
	private ArrayList <Candidate> candidates; 
	
	 // No of votes allowed
	private int voteCount;					 
	

	// Constructor
	public ElectedOffice(String eOffice, ArrayList<Candidate> c, int vCount) {
		ELECTED_OFFICE = eOffice;
		voteCount = vCount;
		
		if (c != null) {
			candidates = c;
		}
		else {
			candidates = new ArrayList<Candidate>();
		}
	}
	
	// Add a new candidate to the list.
	public boolean addCandidate(Candidate candidate){
		for (Candidate c: candidates) {
			if (c.getName().equals(c.getName()) &&
					c.getOffice().equals(candidate.getOffice()) &&
					c.getParty().equals(candidate.getParty())){
				return false;
			}
		}
		
		candidates.add(candidate);
		return true;
	}
	
	// Get the candidate list
	public ArrayList<Candidate> getCandidates() {
		return candidates;
	}
	
	// Get the candidate count
	public int getCandiateCount() {
		return candidates.size();
	}
	 
	// Get the candidate at an index
	public Candidate getCandidateAt(int index) {
		return candidates.get(index);
	}
	
	// Drop a candidate from the list
	public boolean removeCandidateAt(int index){
		
		if (index >=0 && index < candidates.size()) {
			candidates.remove(index);
			return true;
		}
		
		return false;
	}
	
	// Set vote count
	public void setVoteCount(int vCount) {
		voteCount = vCount;
	}
	
	// Get vote count
	public int getVoteCount() {
		return voteCount;
	}
	
	// toString method
	public String toString(){
		String ret =  ELECTED_OFFICE + " (Vote for " + voteCount + "):\n";
		for (Candidate c: candidates) {
			ret += "___ " + c.getName() + "\n";
		}
		return ret;
	}
	
}
