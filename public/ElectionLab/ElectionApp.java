import java.util.ArrayList;
import java.util.Scanner;


public class ElectionApp {
	
	private static ArrayList<Candidate> getPresidentCandidates() {
		ArrayList<Candidate> candidates = new ArrayList<Candidate>();
		candidates.add(new Candidate("Thomas Jefferson", "Congress", "President", false));
		candidates.add(new Candidate("George Washington", "Congress", "President", false));
		candidates.add(new Candidate("Ben Franklin", "Congress", "President", false));
		
		return candidates;
	}
	
	private static ArrayList<Candidate> getSheriffCandidates() {
		ArrayList<Candidate> candidates = new ArrayList<Candidate>();
		candidates.add(new Candidate("Wyatt Earp", "Congress", "Sheriff", false));
		candidates.add(new Candidate("Bill Hickok", "Congress", "Sheriff", false));
		return candidates;
	}
	
	private static ArrayList<Candidate> getSchoolBoardCandidates() {
		ArrayList<Candidate> candidates = new ArrayList<Candidate>();
		candidates.add(new Candidate("Spock", "Congress", "SCHOOL BOARD", false));
		candidates.add(new Candidate("Mary Poppins", "Congress", "SCHOOL BOARD", false));
		candidates.add(new Candidate("Walt Disney", "Congress", "SCHOOL BOARD", false));
		candidates.add(new Candidate("Steven Spielberg", "Congress", "SCHOOL BOARD", false));

		return candidates;
	}
	
	
	private static ArrayList<ElectedOffice> generateElectionOffices() {
		ArrayList<ElectedOffice> electedOffices = new ArrayList<ElectedOffice>();
		
		
		electedOffices.add(new ElectedOffice("PRESIDENT",getPresidentCandidates(),1));
		electedOffices.add(new ElectedOffice("SHERIFF ",getSheriffCandidates(),1));
		electedOffices.add(new ElectedOffice("SCHOOL BOARD",getSchoolBoardCandidates(),2));

		return electedOffices;
	}
	
	public static void main(String []args) {
		Scanner scnr = new Scanner(System.in);
		
		VotingMachine vm = new VotingMachine(generateElectionOffices());
		
		System.out.println("Press any key to continue");
		String response = scnr.nextLine();
				
		while (!response.equals("112233")) {
			System.out.println("Welcome to voting system!");
			Ballot b = vm.getNewBallot();
			for (int oIndex = 0; oIndex < b.getOfficeCount(); oIndex++) {
				System.out.println("VOTE FOR: " + b.getOfficeAt(oIndex).ELECTED_OFFICE);
				for (int cIndex = 0 ; cIndex < b.getCandidateCountForOffice(oIndex); cIndex++) {
					System.out.println(cIndex + "____" + b.getCandidateForOffice(oIndex, cIndex).getName());
				}
				
				System.out.println("Enter " +  b.getOfficeAt(oIndex).getVoteCount() +" Candidates between 0 and " + (b.getCandidateCountForOffice(oIndex) - 1));
				for (int input = 0; input < b.getOfficeAt(oIndex).getVoteCount(); input++) {
					String s = scnr.nextLine();
					int cIndex = Integer.parseInt(s);
					b.castVoteForCandidate(oIndex, cIndex);
				}
			}
			vm.tallyBallot(b);
			System.out.println("Press any key to continue");
			response = scnr.nextLine();
		}
		
		
		for (int oIndex = 0; oIndex < vm.getOfficeCount(); oIndex++) {
			System.out.println(vm.getElectedOfficeAt(oIndex).ELECTED_OFFICE);
			ArrayList<String> names = new ArrayList<>();
			ArrayList<Integer> votes = new ArrayList<>();
			
			for (int cIndex = 0 ; cIndex < vm.getElectedOfficeAt(oIndex).getCandiateCount(); cIndex++) {
				int indexToInsert = 0;
				while (indexToInsert < votes.size() &&
						vm.reportForCandidate(oIndex, cIndex) < votes.get(indexToInsert)) {
					indexToInsert++;
				}
				
				names.add(indexToInsert, vm.getElectedOfficeAt(oIndex).getCandidateAt(cIndex).getName());
				votes.add(indexToInsert, vm.reportForCandidate(oIndex, cIndex));
			}
			
			for (int i = 0; i < names.size(); i++) {
				System.out.println(names.get(i)+ ": " + votes.get(i));
				
			}
		}
		
	}
}
